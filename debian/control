Source: r-cran-choroplethr
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-hmisc,
               r-cran-stringr,
               r-cran-ggplot2,
               r-cran-dplyr,
               r-cran-r6,
               r-cran-wdi,
               r-cran-gridextra,
               r-cran-xml2,
               r-cran-rvest,
               r-cran-tidyr,
               r-cran-acs,
               r-cran-ggmap,
               r-cran-rgooglemaps,
               r-cran-tigris,
               r-cran-tidycensus
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-choroplethr
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-choroplethr.git
Homepage: https://cran.r-project.org/package=choroplethr
Rules-Requires-Root: no

Package: r-cran-choroplethr
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: simplify the creation of choropleth maps in GNU R
 Choropleths are thematic maps where geographic regions, such as
 states, are colored according to some metric, such as the number of people
 who live in that state. This package simplifies this process by 1.
 Providing ready-made functions for creating choropleths of common maps. 2.
 Providing data and API connections to interesting data sources for making
 choropleths. 3. Providing a framework for creating choropleths from
 arbitrary shapefiles. 4. Overlaying those maps over reference maps from
 Google Maps.
